import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          color: black;
        }

        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Sr/a [[id]]</h2>
      <dom-repeat items="{{accounts}}">
      <template>
        <h3>IBAN - {{item.IBAN}}</h3>
        <h3>Saldo - {{item.saldo}}</h3>
        </template>
      </dom-repeat>


      <iron-ajax auto id="getUser"
      url="http://localhost:3000/apitechu/v1/accounts/{{id}}"
      handle-as="json"
      on-response="showData">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      id: {
        type: Number,
      },
      accounts: {
        type: Array
      }
    };
  }// end properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.accounts=data.detail.response;

  }
}//end class

window.customElements.define('visor-cuentas', VisorCuentas);
