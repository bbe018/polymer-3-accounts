import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import'@polymer/iron-ajax/iron-ajax.js'
/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          border: solid red;
          color: black;
        }

        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg{
          background-color: blue;
        }
        .greybg{
          background-color: grey;
        }


      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h2>y mi email es [[email]]</h2>
      <!--
      De las 12 columnas responsive de bootstrap digo cuantas ocupa cada columna creada
      -->
      <div class="row greybg">
        <div class="col-3 offset 1 col-sm-4 redbg">Col 1</div>
        <div class="col-4 col-sm-3 greebg">Col 2</div>
        <div class="col-5 col-sm-4 bluebg">Col 3</div>
      </div>

      <button class="btn btn-success btn-lg">Login</button>
      <button class="btn btn-info btn-sm">Logout</button>
      <button class="btn btn-success">Login</button>
      <button class="btn btn-danger">Logout</button>
      <button class="btn btn-light">Login</button>
      <button class="btn btn-dark">Logout</button>


      <iron-ajax auto id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      on-response="showData">
      </iron-ajax>

    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String,
      },
      id: {
        type: Number,
      },
      last_name: {
        type: String,
      },
      email: {
        type: String,
      }
    };
  }// end properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name=data.detail.response.first_name
    this.last_name=data.detail.response.last_name
    this.email=data.detail.response.email
  }
}//end class

window.customElements.define('visor-usuario', VisorUsuario);
